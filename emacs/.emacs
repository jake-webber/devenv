
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
(load-theme 'zenburn t)

;; set backup files to go to a specific directory
(setq backup-directory-alist `(("." . "~/.saves")))

;; using exec-path-from-shell extension, invoke it
;; so that we have shell variables from iterm available
;; in the emacs shell (needed for phantomjs on path
(exec-path-from-shell-copy-env "EWAY_API_KEY")
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

(require 'powerline)
(powerline-default-theme)

(tool-bar-mode -1)
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(scroll-bar-mode -1)
(global-linum-mode t)

(require 'yasnippet)
(yas-global-mode 1)


(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))

;; allow for inf ruby to be used when rspeccing so we can byebug tests.
(add-hook 'after-init-hook 'inf-ruby-switch-setup)

(projectile-rails-global-mode)

(eval-after-load 'rspec-mode
  '(rspec-install-snippets))

;; Magit rules!
(global-set-key (kbd "C-x g") 'magit-status)

(setenv "SHELL" "/bin/bash")

(require 'powerline)


(require 'helm-projectile)
(helm-projectile-on)

(global-set-key (kbd "C-,") 'helm-projectile)

(require 'rbenv)
(global-rbenv-mode)

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")
(add-to-list 'load-path "~/.emacs.d/evil") ;;no need with 24

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
(global-set-key (kbd "C-x C-f") #'helm-find-files)

(require 'evil)
(require 'helm-config)
(evil-mode 1)

(require 'rspec-mode)
(defadvice rspec-compile (around rspec-compile-around)
  "Use BASH shell for running the specs because of ZSH issues."
  (let ((shell-file-name "/bin/bash"))
    ad-do-it))
(windmove-default-keybindings)

(setq compilation-scroll-output t)

(ad-activate 'rspec-compile)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("c968804189e0fc963c641f5c9ad64bca431d41af2fb7e1d01a2a6666376f819c" "760ce657e710a77bcf6df51d97e51aae2ee7db1fba21bbad07aab0fa0f42f834" "1263771faf6967879c3ab8b577c6c31020222ac6d3bac31f331a74275385a452" "34ed3e2fa4a1cb2ce7400c7f1a6c8f12931d8021435bad841fdc1192bd1cc7da" "8be07a2c1b3a7300860c7a65c0ad148be6d127671be04d3d2120f1ac541ac103" "aea30125ef2e48831f46695418677b9d676c3babf43959c8e978c0ad672a7329" "f5512c02e0a6887e987a816918b7a684d558716262ac7ee2dd0437ab913eaec6" "1e67765ecb4e53df20a96fb708a8601f6d7c8f02edb09d16c838e465ebe7f51b" default)))
 '(package-selected-packages
   (quote
    (web-mode helm-ag base16-theme exec-path-from-shell rubocop git-gutter magit yasnippet powerline projectile-rails helm-projectile zenburn-theme paganini-theme rbenv helm-R ## helm rspec-mode evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
