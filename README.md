cd
git clone git@bitbucket.org:jake-webber/devenv.git devenv
ln -s `pwd`/devenv/.vimrc ~/.vimrc
vim -c 'PlugInstall'
cd -
