export PATH="$HOME/.rbenv/shims:$PATH"
export ZSH=/Users/jake/.oh-my-zsh
ZSH_THEME="wezm"
plugins=(git)
source $ZSH/oh-my-zsh.sh
