let g:netrw_bufsettings = "noma nomod nu nobl nowrap ro nornu"
let mapleader=","

map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>

let g:vimrubocop_keymap = 0
nmap <Leader>r :RuboCop<CR>

" search and replace
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

" use tab and shift + tab to go forward and back buffers
nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

nnoremap <leader>. :OpenAlternate<cr>

set t_Co=256

set backspace=indent,eol,start
set cursorline
set cursorcolumn

set incsearch

" syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 1
let g:syntastic_ruby_exec = '/Users/jakewebber/.rbenv/shims/ruby'
let g:syntastic_ruby_checkers          = ['mri']

autocmd VimEnter * command! -nargs=* Ag
      \ call fzf#vim#ag(<q-args>, '--color-path "3;32" --color-match "30;41" --color-line-number "2;38"', fzf#vim#default_layout)

call plug#begin('~/.vim/plugged')
Plug 'https://github.com/chriskempson/base16-vim.git'
Plug 'https://github.com/mattn/emmet-vim.git'
Plug 'https://github.com/thoughtbot/vim-rspec'
Plug 'https://github.com/vim-airline/vim-airline'
Plug 'https://github.com/junegunn/fzf'
Plug 'https://github.com/junegunn/fzf.vim'
Plug 'thoughtbot/vim-rspec'
Plug 'mileszs/ack.vim'
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'airblade/vim-gitgutter'
Plug 'https://github.com/tpope/vim-fugitive'
Plug 'https://github.com/tpope/vim-endwise.git'
Plug 'https://github.com/tpope/vim-ragtag'
Plug 'https://github.com/ecomba/vim-ruby-refactoring'
Plug 'https://github.com/tmhedberg/matchit'
Plug 'https://github.com/uptech/vim-open-alternate'
Plug 'https://github.com/terryma/vim-multiple-cursors'
Plug 'https://github.com/ngmy/vim-rubocop'
Plug 'https://github.com/vim-syntastic/syntastic'
Plug 'https://github.com/tpope/vim-rails'
call plug#end()
set number
set tabstop=2
set shiftwidth=2
set laststatus=2
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

set foldmethod=syntax
set nofoldenable

set nocompatible      " We're running Vim, not Vi!
syntax on             " Enable syntax highlighting
filetype on           " Enable filetype detection
filetype indent on    " Enable filetype-specific indenting
filetype plugin on    " Enable filetype-specific plugins

autocmd FileType ruby setlocal expandtab shiftwidth=2 tabstop=2
autocmd FileType eruby setlocal expandtab shiftwidth=2 tabstop=2
colorscheme base16-default-dark

nnoremap <silent> <Leader><Leader> :Files<CR>
nnoremap <silent> <Leader><Enter> :Buffers<CR>
nnoremap <silent> <Leader>ag :Ag <C-R><C-W><CR>
nnoremap <silent> <Leader>AG :Ag <C-R><C-A><CR>
nnoremap <silent> <Leader>` :Marks<CR>

imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)
